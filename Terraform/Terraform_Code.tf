### Terraform Code ###


module "splunk-sa-legacy" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "Splunk SAS"
  associate_public_ip_address = true
  ami                    = var.ami
  instance_type          = var.splunk_instance_type
  key_name               = var.key_name
  monitoring             = false
  vpc_security_group_ids = var.sec_grps
  subnet_id              = var.subnet_id
  #private_ip            = "172.16.1.20"
  iam_instance_profile   = "ec2-user"

  # Storage Settings

  root_block_device = [
     {
       volume_size = 256
     },
   ]

  # Tags

  tags = {
     Name                 = "Splunk SA Legacy"
     splunk_instance_type = "standalone"
     application          = "splunk"
     owner                = "Tony Nesavich"
     environment          = "Windows-legacy-env"
     splunk_ui_label      = "Legacy Dev Instance"
  }

}


module "splunk-sa-CP" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "Splunk SAS"
  associate_public_ip_address = true
  ami                    = var.ami
  instance_type          = var.splunk_instance_type
  key_name               = var.key_name
  monitoring             = false
  vpc_security_group_ids = var.sec_grps
  subnet_id              = var.subnet_id
  #private_ip            = "172.16.1.20"
  iam_instance_profile   = "ec2-user"

  # Storage Settings

  root_block_device = [
     {
       volume_size = 256
     },
   ]

  # Tags

  tags = {
     Name                 = "Splunk SA CP"
     splunk_instance_type = "standalone"
     application          = "splunk"
     owner                = "Tony Nesavich"
     environment          = "Windows-cp-env"
     splunk_ui_label      = "CP Dev Instance"
  }

}

module "Windows-DC-Legacy" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "DC SAS"
  associate_public_ip_address = true
  ami                    = var.dcami
  instance_type          = var.instance_type
  key_name               = var.key_name
  monitoring             = false
  vpc_security_group_ids = var.sec_grps
  subnet_id              = var.subnet_id
  #private_ip            = "172.16.1.20"
  iam_instance_profile   = "ec2-user"

  # Storage Settings

  root_block_device = [
     {
       volume_size = 256
     },
   ]

  # Tags

  tags = {
     Name                 = "Windows DC Legacy"
     splunk_instance_type = "standalone"
     application          = "splunk"
     owner                = "Tony Nesavich"
     environment          = "Windows-legacy-env"
     splunk_ui_label      = "Legacy Dev Instance"
  }

}

module "Windows-DC-CP" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "DC SAS"
  associate_public_ip_address = true
  ami                    = var.dcami
  instance_type          = var.instance_type
  key_name               = var.key_name
  monitoring             = false
  vpc_security_group_ids = var.sec_grps
  subnet_id              = var.subnet_id
  #private_ip            = "172.16.1.20"
  iam_instance_profile   = "ec2-user"

  # Storage Settings

  root_block_device = [
     {
       volume_size = 256
     },
   ]

  # Tags

  tags = {
     Name                 = "Windows DC CP"
     splunk_instance_type = "standalone"
     application          = "splunk"
     owner                = "Tony Nesavich"
     environment          = "Windows-cp-env"
     splunk_ui_label      = "CP Dev Instance"
  }

}

module "Windows-Client-Legacy" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "DC SAS"
  associate_public_ip_address = true
  ami                    = var.client_ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  monitoring             = false
  vpc_security_group_ids = var.sec_grps
  subnet_id              = var.subnet_id
  #private_ip            = "172.16.1.20"
  iam_instance_profile   = "ec2-user"

  # Storage Settings

  root_block_device = [
     {
       volume_size = 256
     },
   ]

  # Tags

  tags = {
     Name                 = "Windows Client Legacy"
     splunk_instance_type = "standalone"
     application          = "splunk"
     owner                = "Tony Nesavich"
     environment          = "Windows-legacy-env"
     splunk_ui_label      = "Legacy Dev Instance"
  }

}

module "Windows-Client-CP" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "DC SAS"
  associate_public_ip_address = true
  ami                    = var.client_ami
  instance_type          = var.instance_type
  key_name               = var.key_name
  monitoring             = false
  vpc_security_group_ids = var.sec_grps
  subnet_id              = var.subnet_id
  #private_ip            = "172.16.1.20"
  iam_instance_profile   = "ec2-user"

  # Storage Settings

  root_block_device = [
     {
       volume_size = 256
     },
   ]

  # Tags

  tags = {
     Name                 = "Windows Client CP"
     splunk_instance_type = "standalone"
     application          = "splunk"
     owner                = "Tony Nesavich"
     environment          = "Windows-cp-env"
     splunk_ui_label      = "CP Dev Instance"
  }

}