### Variables File ###


variable "key_name" {
  type = string
  default = "SplunkMain"
}
variable "subnet_id" {
  type = string
  default = "subnet-e87b7681"
}
variable "sec_grps" {
  type = list(string)
  default = [ "sg-000ed06b" ]
}
variable "ami" {
  type = string
  default = "ami-0ba62214afa52bec7"
}
variable "dcami" {
  type = string
  default = "ami-0c4a11a8d0e503812"
}
variable "client_ami" {
  type = string
  default = "ami-04344d607c1a9d591"
}
variable "instance_type" {
  type = string
  default = "t2.xlarge"
}
variable "splunk_instance_type" {
  type = string
  default = "c5.2xlarge"
}